#IfWinActive ahk_class QWidget

^!Ins::
	; ControlGet, codeBlock, Selected, ,Edit, Add ahk_exe anki.exe
	; ControlGet, codeBlock, Selected, ,Edit1, ahk_class QWidget
	Send ^x
	
	Send {Space}{Space}{Space}{Space}`n
	Send {Space}{Space}{Space}{Space}::java`n
	Loop, Parse, Clipboard, `n, 
		{
			Send {Space}{Space}{Space}{Space}%A_LoopField%
		}
	Send `n
	Send {Space}{Space}{Space}{Space}
	Send +^m
	Return